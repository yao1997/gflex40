<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticipantForm extends Model
{
    protected $table = 'participant_forms';
    protected $guarded = ['id'];

}
