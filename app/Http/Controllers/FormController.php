<?php

namespace App\Http\Controllers;

use App\NominationForm;
use App\ParticipantForm;
use Illuminate\Http\Request;

class FormController extends Controller
{

    //Form page
    public function index(){
        return view('pages.form');
    }


    //Submit form : create new record
    public function submit(Request $request){
        $participant_data = [
            'name' =>  $request->name,
            'sex' =>  $request->sex,
            'marital_status' =>  $request->marital_status,
            'age' =>  $request->age,
            'occupation' =>  $request->occupation,
            'ic' =>  $request->ic,
            'mobile_phone_num' =>  $request->mobile_phone_num,
            'fixed_phone_num' =>  $request->fixed_phone_num,
            'height' =>  $request->height,
            'weight' =>  $request->weight,
            'num_children' =>  $request->num_children?$request->num_children:0,
            'address' =>  $request->address,
            'major_surgery' =>  $request->major_surgery,
            'spouse_name' =>  $request->spouse_name,
            'spouse_sex' =>  $request->spouse_sex,
            'spouse_age' =>  $request->spouse_age,
            'spouse_occupation' =>  $request->spouse_occupation,
            'spouse_ic' =>  $request->spouse_ic,
            'spouse_mobile_phone_num' =>  $request->spouse_mobile_phone_num,
            'spouse_fixed_phone_num' =>  $request->spouse_fixed_phone_num,
            'spouse_height' =>  $request->spouse_height,
            'spouse_weight' =>  $request->spouse_weight,
            'pre_existing_cond' =>  $request->pre_existing_cond,
        ];

        $participant_form = ParticipantForm::create($participant_data);


        $nominee1_data = [
            'participant_form_id' => $participant_form->id,
            'name' => $request->nominee_1_name,
            'sex' => $request->nominee_1_sex,
            'age' => $request->nominee_1_age,
            'address' => $request->nominee_1_address,
            'ic' => $request->nominee_1_ic,
            'mobile_phone_num' => $request->nominee_1_mobile_phone_num,
            'fixed_phone_num' => $request->nominee_1_fixed_phone_num,
            'relation' => $request->nominee_1_relation,
            'policy_payout' => $request->nominee_1_policy_payout
        ];
        $nominee1_form = NominationForm::create($nominee1_data);

        $nominee2_data = [
            'participant_form_id' => $participant_form->id,
            'name' => $request->nominee_2_name,
            'sex' => $request->nominee_2_sex,
            'age' => $request->nominee_2_age,
            'address' => $request->nominee_2_address,
            'ic' => $request->nominee_2_ic,
            'mobile_phone_num' => $request->nominee_2_mobile_phone_num,
            'fixed_phone_num' => $request->nominee_2_fixed_phone_num,
            'relation' => $request->nominee_2_relation,
            'policy_payout' => $request->nominee_2_policy_payout
        ];
        $nominee2_form = NominationForm::create($nominee2_data);

        return redirect('/')->with('success', "Form Submitted");
    }
}
