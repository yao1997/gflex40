<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscriptions;

class IndexController extends Controller
{
    public function index(){
        return view('pages.welcome_index');
    }

    public function subscribe(Request $request){
        $validatedData = $request->validate([
            'email' => 'required|email|unique:subscriptions'
        ]);
        $subscription=new Subscriptions;
        $subscription->email=$request->email;
        $subscription->save();
        return back()->with('success', "Register Successfully");
    }
}
