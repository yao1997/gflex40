<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NominationForm extends Model
{
    protected $table = 'nomination_forms';
    protected $guarded = ['id'];

}
