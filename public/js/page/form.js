$(document).ready(function(){
    $(document).on('change','.marital-radio',function(){
        var status = $(this).val();
        if(status!= 0 )
        {
            $('.spouse-area').fadeIn();
            $('.childRequired').addClass('required');
            if(status != 1)
            {
                $('.spouseRequired').removeClass('required');
                $('.spouse-box').fadeOut();
            }
            else
            {
                $('.spouseRequired').addClass('required');
                $('.spouse-box').fadeIn();
            }
        }
        else
        {
            $('.childRequired').removeClass('required');
            $('.spouseRequired').removeClass('required');
            $('.spouse-area').fadeOut();
        }

    });


    (function($) {

        var form = $("#signup-form");
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            rules: {
                confirm: {
                    equalTo: "#name"
                }
            }
        });
        form.steps({
            headerTag: "h3",
            bodyTag: "fieldset",
            transitionEffect: "fade",
            enableAllSteps: true,
            labels: {
                previous : 'Prev',
                next : 'Next',
                finish : 'Finish',
                current : ''
            },
            titleTemplate : '<h3 class="title">#title#</h3>',
            onStepChanging: function (event, currentIndex, newIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                $('.signup-form').submit();
            }
        });

        $('#country').parent().append('<ul id="newcountry" class="select-list" name="sex"></ul>');
        $('#country option').each(function(){
            $('#newcountry').append('<li value="' + $(this).val() + '">'+$(this).text()+'</li>');
        });
        $('#country').remove();
        $('#newcountry').attr('id', 'country');
        $('#country li').first().addClass('init');
        $("#country").on("click", ".init", function() {
            $(this).closest("#country").children('li:not(.init)').toggle();
        });

        var allOptions = $("#country").children('li:not(.init)');
        $("#country").on("click", "li:not(.init)", function() {
            allOptions.removeClass('selected');
            $(this).addClass('selected');
            $("#country").children('.init').html($(this).html());
            allOptions.toggle();
        });

        $('#country2').parent().append('<ul id="newcountry2" class="select-list" name="spouse_sex"></ul>');
        $('#country2 option').each(function(){
            $('#newcountry2').append('<li value="' + $(this).val() + '">'+$(this).text()+'</li>');
        });
        $('#country2').remove();
        $('#newcountry2').attr('id', 'country2');
        $('#country2 li').first().addClass('init');
        $("#country2").on("click", ".init", function() {
            $(this).closest("#country2").children('li:not(.init)').toggle();
        });

        var allOptions2 = $("#country2").children('li:not(.init)');
        $("#country2").on("click", "li:not(.init)", function() {
            allOptions2.removeClass('selected');
            $(this).addClass('selected');
            $("#country2").children('.init').html($(this).html());
            allOptions2.toggle();
        });



        $('#country3').parent().append('<ul id="newcountry3" class="select-list" name="spouse_sex"></ul>');
        $('#country3 option').each(function(){
            $('#newcountry3').append('<li value="' + $(this).val() + '">'+$(this).text()+'</li>');
        });
        $('#country3').remove();
        $('#newcountry3').attr('id', 'country3');
        $('#country3 li').first().addClass('init');
        $("#country3").on("click", ".init", function() {
            $(this).closest("#country3").children('li:not(.init)').toggle();
        });

        var allOptions3 = $("#country3").children('li:not(.init)');
        $("#country3").on("click", "li:not(.init)", function() {
            allOptions3.removeClass('selected');
            $(this).addClass('selected');
            $("#country3").children('.init').html($(this).html());
            allOptions3.toggle();
        });



        $('#country4').parent().append('<ul id="newcountry4" class="select-list" name="spouse_sex"></ul>');
        $('#country4 option').each(function(){
            $('#newcountry4').append('<li value="' + $(this).val() + '">'+$(this).text()+'</li>');
        });
        $('#country4').remove();
        $('#newcountry4').attr('id', 'country4');
        $('#country4 li').first().addClass('init');
        $("#country4").on("click", ".init", function() {
            $(this).closest("#country4").children('li:not(.init)').toggle();
        });

        var allOptions4 = $("#country4").children('li:not(.init)');
        $("#country4").on("click", "li:not(.init)", function() {
            allOptions4.removeClass('selected');
            $(this).addClass('selected');
            $("#country4").children('.init').html($(this).html());
            allOptions4.toggle();
        });


    })(jQuery);
})
