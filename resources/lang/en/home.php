<?php

return [

    'home' => 'Home',
    'about_us' => 'About Us',
    'what_is_takaful' => 'What is Takaful',
    'register' => 'Register',
    'introduction' => "GFlex40 is the World's First Virtual Takaful and we are proudly Malaysian!",
    'introduction_description' => 'If you are looking for affordable life coverage or coverage that grows with your needs and is Shahria compliant, then choose GFlex40!',
    'for_all_malaysian' => 'GFlex40 is a Family Takaful that is for all Malaysians.',

    'about_us_1'=>'GFlex40 is Malaysia’s and probably the World’s first Virtual Takaful. Proudly Malaysian founded and developed, GFlex40 intends to make Takaful affordable for Malaysians of walks of life. Our products are easily accessible, especially affordable for the B40 and more importantly completely Sharia compliant',
    'about_us_2'=>'Whether you are a young Malaysian Professional stepping into the working world or a young couple building a family together or even a business lady trying to secure a future, our products are tailor made for you',
    'about_us_3'=>'So wherever you are in Malaysia, just log on to www.gflex40.com and get yourself covered the easy way. ',
    'about_us_4'=>'What more, we have also prepared various easily accesible payment modes for you. You can even pay through your monthly phone bill. ',

    'what_is_takaful_page'=> 'Takaful is a type of Islamic mutual protection that is Syariah compliant and provides assistance to its members in facing life’s challenges such as death of a loved one.A Takaful operates by setting up a fund into which the members contribute monthly. When there is a loss such as death, the pre-agreed sum is paid to the nominated recipient by the Takaful Wakeel (Operator) ',

    'pricing_plan' => 'Our pricing plans',
    'pricing_plan_description' => 'GFlex40 is a Shahria compliant Wakf based Takaful product which is affordable, easy to access and flexible. It grows with you and your lifestyle so that you can always opt for higher value plan with a simple upgrade process',
    'pricing_plan_description2' => 'So what are you waiting for, learn more about GFlex40 and sign up to be an early bird when we launch! ',


    'as_low_as_10' => 'As low as RM10',
    'as_low_as_30' => 'As low as RM30',
    'as_low_as_60' => 'As low as RM60',
    'per_month' => '/mo',

    'starter_pack' => 'Starter Takaful Pack',
    'family_pack' => 'Family Takaful Pack',
    'professional_pack' => 'Family Takaful Pack',

    'RM30000' => 'RM30,000 at passing of Participant',
    'RM50000' => 'RM50,000 at passing of Participant',
    'RM100000' => 'RM100,000 at passing of Participant',

    'immediate_approval' => 'immediate approval and coverage',
    'flexible' => 'Flexible payment through Phone Bill, Bank or Local Post Office',
    'simple' => 'Simple claim procedure direct from Phone or Computer',

    'get_started' => 'Get Started',
    'sign_up_now' => 'Sign Up Now',

    'why_us' => 'Why Us?',
    'world_first' => "World's First Virtual Takaful.",
    'world_first_description' => 'We are the first virtual takaful in Malaysia, and the first in the world.',
    'fully_sharia_compliant' => 'Takaful Virtual Pertama Di Dunia',
    'fully_sharia_compliant_desciption' => 'Immediate processing, approval and coverage',
    'immediate_approval' => 'All the processes are handled online, and hence it can be incredibly fast and responsive.',
    'immediate_approval_description' => 'All the processes are handled online, and hence it can be incredibly fast and responsive.',
    'affordable' => 'Affordable',
    'affordable_description' => 'We are affordable for every citizens in Malaysia.',
    'easy_to_access' => 'Extremely convenient',
    'easy_to_access_description' => 'Claims made directly online with simple snap of a picture of necessary documents',
    'convenient' => 'Mempermudahkan saat-saat duka dan sukar dalam hidup',
    'convenient_desciption' => 'Claims made directly online with simple snap of a picture of necessary documents',
    'easy_payment' => 'Easy Payment',
    'easy_payment_desciption' => 'Multiple payment modes available from Phone bill deduction (post & prepaid) to Bank transfer and Post Office payment',
    'flexible' => 'Flexible',
    'flexible_desciption' => 'GFlex40 is a Flexi Takaful which when you can afford it, you can increase your coverage or when you cant, you can reduce it.',

    'register_now' => 'Register Form',
    'please_fill_in_all_field' => 'Please fill in all the input field',
    'personal' => 'Personal',
    'your_name' => 'Your Name',
    'gender' =>'Gender',
    'occupation' => 'Occupation',
    'ic_no' => 'IC No.',
    'mobile_no' => 'Mobile Phone No. ',
    'fix_phone_no' => 'Fixed Phone No.',
    'age' => 'Age',
    'height' => 'Height (cm)',
    'weight' => 'Weight (kg) ',
    'major_surgery_in_past' => 'Major surgery in past 6 years',
    'pre_existing' => 'Pre-existing Condition',
    'address' => 'Address',

    'next' => 'Next',
    'prev' => 'Previous',

    'male' => 'Male',
    'female' => 'Female',

    'spouse' => 'Spouse',
    'marital_status' => 'Marital Status',
    'single' => 'Single',
    'married' => 'Married',
    'no_of_children' => 'Number of children',
    'spouse_name' => 'Spouse Name',
    'spouse_gender' => 'Spouse Gender',
    'spouse_occupation' => 'Spouse Occupation',
    'spouse_ic' => 'Spouse IC No.',
    'spouce_mobile_no' => 'Spouse Mobile Phone No.',
    'spouse_fix_phone_no' => 'Spouse Fixed Phone No.',
    'spouse_age' => 'Spouse Age',
    'spouse_height' => 'Spouse Height (cm)',
    'spouse_weight' => 'Spouse Weight (kg)',
    'spouse_pre_condition' => 'Pre-existing Condition',

    'divorced'=>'Divorced',
    'separated' => 'Seperated',

    'nomination' => 'Nomination',
    'nominee_1' => 'Nominee 1',
    'nominee_2' => 'Nominee 2',
    'name' => 'name',
    'relation_to_insured' => 'Relationship to Insured ',
    'percent_payout' => '% of sharing',

    'finish' => 'Finish',
];
