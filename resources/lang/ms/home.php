<?php

return [

    'home' => 'Laman Utama',
    'about_us' => 'Mengenai Kami',
    'what_is_takaful' => 'Apakah itu Takaful',
    'register' => 'Pendaftaran',
    'introduction' => 'GFlex40 merupakan Takaful Virtual yang pertama di Dunia dan merupakan Produk Malaysia 100%',
    'introduction_description' => 'Adakah anda sedang mencari atau berminat dengan Takaful Keluarga yang fleksibel, berubah selaras gaya hidup anda serta memenuhi peraturan Syariah? Sekiranya ya, pilihlah GFlex40',
    'for_all_malaysian' => 'GFlex40 merupakan Takaful Keluarga untuk serata Rakyat Malaysia',

    'about_us_1'=>'GFlex40 merupakan Takaful Virtual pertama di Malaysia dan juga di Dunia. Kami ingin menyediakan untuk Rakyat Malaysia produk Takaful yang mampu milik, mudah dikecapi dan paling penting mengikut Syariah',
    'about_us_2'=>'Sekiranya anda seorang graduan baru yang sedang menceburi bidang kerja, keluarga muda yang menunggu cahaya mata atau juga seorang ibu atau isteri yang menjalankan perusahaan sendiri, produk kami sesuai untuk anda.',
    'about_us_3'=>'Jadi, di mana jua anda berada di Malaysia, layarilah www.gflex40.com dan dapatkan Takaful dengan cara yang mudah.',
    'about_us_4'=>'Apa lagi, kami sudah pun menyiapkan pelbagai mod pembayaran yang mudah bagi anda termasuk pembayaran melalui bil telefon bimbit.  ',

    'what_is_takaful_page'=> 'Takaful adalah sejenis perlindungan bersama patuh Syariah yang boleh membantu para ahli perlindungan tersebut menghadapi cabaran-cabaran hidup seperti kehilangan mereka yang tersayang. Takaful beroperasi dengan mod dimana satu dana umum disiapkan oleh Wakeel (operator) dan para ahli menyumbang kepada dana tersebut. Sekiranya berlaku kematian, pampansan akan dilakukan dari amaun yang tersedia dari dana tersebut. ',

    'pricing_plan' => 'Senarai Harga',
    'pricing_plan_description' => 'GFlex40 merupakan produk Takaful berdasarkan prinsip Waqf. Ianya produk yang mampu milik, mudah dicari dan fleksibel. Ia juga sebuah produk yang boleh berubah dengan gaya hidup serta kemampuan anda',
    'pricing_plan_description2' => 'Jangan tunggu lagi. Ketahuilah lebih lanjut mengenai GFlex40 dan daftarkan diri untuk menjadi antara pelanggan pertama kami ketika pelancaran! ',


    'as_low_as_10' => 'Serendah RM10',
    'as_low_as_30' => 'Serendah RM30',
    'as_low_as_60' => 'Serendah RM60',
    'per_month' => '/bulan',

    'starter_pack' => 'Pek Permulaan Takaful',
    'family_pack' => 'Pek Keluarga Takaful',
    'professional_pack' => 'Pek Professional Takaful',

    'RM30000' => 'Wang Pampasan RM30,000',
    'RM50000' => 'Wang Pampasan RM50,000',
    'RM100000' => 'Wang Pampasan RM100,000',

    'immediate_approval' => 'Kelulusan dan perlindungan segera',
    'flexible' => 'Mod pembayaran fleksibel melalui bil telefon, Kaunter Bank atau Pejabat Pos',
    'simple' => 'Prosedur tuntutan mudah melalui telefon bimbit atau komputer',

    'get_started' => 'Mulakan Sekarang',
    'sign_up_now' => 'Daftarkan Diri',

    'why_us' => 'Kenapa pilih kami?',
    'world_first' => 'Takaful Virtual Pertama Di Dunia.',
    'word_first_description' => 'Kami merupakan Takaful Virtual pertama di Malaysia dan Dunia',
    'fully_sharia_compliant' => 'Diiktiraf oleh Lembaga Syariah terkemuka',
    'fully_sharia_compliant_desciption' => 'GFlex40 merupakan produk yang diiktiraf oleh Lembaga Syariah terkemuka',
    'immediate_approval' => 'Kelulusan dan perlindungan segera',
    'immediate_approval_description' => 'Segala proses berkait dilakukan secara atas talian dan memperbolehkan GFlex40 melakukan proses kelulusan dan perlindungan yang begitu cepat',
    'affordable' => 'Produk Mampu Milik',
    'affordable_description' => 'GFlex40 merupakan produk mampu milik yang boleh dimiliki oleh semua Rakyat Malaysia',
    'easy_to_access' => 'Mudah dimiliki',
    'easy_to_access_description' => 'GFlex40 boleh diakses dan dimiliki secara mudah melalui mana-mana telefon bimbit atau komputer',
    'convenient' => 'Mempermudahkan saat-saat duka dan sukar dalam hidup',
    'convenient_desciption' => 'Segala tuntutan dapat dilakukan secara atas talian dengan hanya memperlengkapkan borang dan melampirkan foto dokumen',
    'easy_payment' => 'Mod pembayaran pelbagai',
    'easy_payment_desciption' => 'GFlex40 menyediakan pelbagai mod pembayaran seperti pemotongan dari bil telefon, transfer bank serta pembayaran di Pejabat Pos',
    'flexible' => 'Flexible',
    'flexible_desciption' => 'GFlex40 adalah sebuah Flexi Takaful. Jadi apabila anda mampu, anda boleh meningkatkan tahap perlindungan atau sekiranya anda ingin, anda juga boleh mengurangkannya',

    'register_now' => 'Borang Pendaftaran',
    'please_fill_in_all_field' => 'Sila lengkapkan yang mana perlu',
    'personal' => 'Peribadi',
    'your_name' => 'Nama Anda',
    'gender' =>'Jantina',
    'occupation' => 'Pekerjaan',
    'ic_no' => 'No. Kad Pengenalan',
    'mobile_no' => 'No. Telefon Bimbit',
    'fix_phone_no' => 'No. Telefon Rumah',
    'age' => 'Umur',
    'height' => 'Ketinggian (cm)',
    'weight' => 'Berat (kg)',
    'major_surgery_in_past' => 'Adakah anda pernah melalui apa-apa pembedahan besar dalam tempoh 6 tahun terdekat?',
    'pre_existing' => 'Kondisi-kondisi pra-daftar',
    'address' => 'Alamat',

    'next' => 'Lanjut',
    'prev' => 'Kembali',

    'male' => 'Lelaki',
    'female' => 'Perempuan',

    'spouse' => 'Pasangan',
    'marital_status' => 'Status Perkahwinan',
    'single' => 'Bujang',
    'married' => 'Sudah Berkahwin',
    'no_of_children' => 'Bilangan anak-anak',
    'spouse_name' => 'Nama Suami/Isteri',
    'spouse_gender' => 'Jantina Pasangan',
    'spouse_occupation' => 'Pekerjaan Pasangan',
    'spouse_ic' => 'No. Kad Pengenalan Pasangan',
    'spouce_mobile_no' => 'No. Telefon Bimbit Pasangan',
    'spouse_fix_phone_no' => 'No. Telefon Rumah Pasangan',
    'spouse_age' => 'Umur Pasangan',
    'spouse_height' => 'Ketinggian (cm) Pasangan',
    'spouse_weight' => 'Berat (kg) Pasangan',
    'spouse_pre_condition' => 'Kondisi-kondisi pra-daftar Pasangan',

    'divorced'=>'Sudah Bercerai',
    'separated' => 'Berpisah',

    'nomination' => 'Nominasi Waris',
    'nominee_1' => 'Waris Pertama',
    'nominee_2' => 'Waris Kedua',
    'name' => 'Nama',
    'relation_to_insured' => 'Hubungan dengan Ahli Takaful',
    'percent_payout' => '% pembahagian',

    'finish' => 'Siap',
];
