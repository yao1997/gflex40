@extends("layout.welcome_layout")

@section('head')
    <link href="/css/page/form.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/plugin/material-design-iconic-font.min.css">

    <script type="text/javascript" src="/js/plugin/jquery.min.js{{ config('app.link_version') }}"></script>
    <script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js'></script>
    <script type="text/javascript" src="/js/plugin/jquery.steps.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/page/form.js{{ config('app.link_version') }}"></script>
@endsection


@section('content')


<main>
  <div class="container">

      <section class="pt-5">

        <h2 class="my-5 h3 text-left form-title">{{trans('home.register_now')}} </h2>
        <small class='small-title'> {{trans('home.please_fill_in_all_field')}}  </small>


        <div class="main row">

            <div class="container">
                {!! Form::open(['route' => 'form.submit','id' => 'signup-form', 'class' => 'signup-form']) !!}
                    <h3>
                        {{trans('home.personal')}}
                    </h3>
                    <fieldset>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="name" class='required-span'>{{trans('home.your_name')}}</label>
                                <input type="text" name="name" id="name" class="required"/>
                            </div>
                            <div class="form-group">
                                <label for="sex" class='required-span'>{{trans('home.gender')}}</label>
                                <select name="sex" id="sex" class='form-group select-form required'>
                                    <option value=""></option>
                                    <option value="0">{{trans('home.male')}}</option>
                                    <option value="1">{{trans('home.female')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">

                            <div class="form-group">
                                <label for="occupation" class='required-span'>{{trans('home.occupation')}}  </label>
                                <input type="text" name="occupation" id="occupation" class="required" />
                            </div>
                            <div class="form-group">
                                <label for="ic" class='required-span'>{{trans('home.ic_no')}}</label>
                                <input type="text" name="ic" id="ic" class="required"/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="mobile_phone_num" class='required-span'>{{trans('home.mobile_no')}} </label>
                                <input type="tel" name="mobile_phone_num" id="mobile_phone_num"  class="required"/>
                            </div>
                            <div class="form-group">
                                <label for="fixed_phone_num" class='required-span'>{{trans('home.fix_phone_no')}}</label>
                                <input type="tel" name="fixed_phone_num" id="fixed_phone_num" class="required" />
                            </div>
                        </div>

                        <div class="form-row multi-col-section">
                            <div class="form-group col-md-4">
                                <label for="age" class='required-span'>{{trans('home.age')}}</label>
                                <input type="Number" name="age" id="age" class="required"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="height" class='required-span'>{{trans('home.height')}} </label>
                                <input  type="number" name="height" id="height" class="required"/>
                            </div>
                            <div class="form-select col-md-4">
                                <label for="weight" class='required-span'>{{trans('home.weight')}} </label>
                                <input  type="number" name="weight" id="weight" class="required"/>
                            </div>
                        </div>
                        <div class="form-textarea address-field">
                            <label for="room_description" class="radio-label required-span">{{trans('home.major_surgery_in_past')}}</label>
                            <textarea name="major_surgery" id="room_description" class="required"></textarea>
                        </div>
                        <div class="form-textarea address-field">
                            <label for="address" class="radio-label required-span">{{trans('home.address')}}</label>
                            <textarea name="address" id="address" class="required"></textarea>
                        </div>
                    </fieldset>

                    <h3>
                        {{trans('home.spouse')}}
                    </h3>
                    <fieldset>
                        <div class="form-radio">
                            <label for="marital_status" class="radio-label required-span">{{trans('home.marital_status')}}</label>
                            <div class="form-radio-group">
                                <div class="form-radio-item">
                                    <input type="radio" name="marital_status" id="single" value='0' class='marital-radio' checked>
                                    <label for="single">{{trans('home.single')}}</label>
                                    <span class="check"></span>
                                </div>
                                <div class="form-radio-item">
                                    <input type="radio" name="marital_status" id="married" value='1' class='marital-radio'>
                                    <label for="married">{{trans('home.married')}}</label>
                                    <span class="check"></span>
                                </div>
                                <div class="form-radio-item">
                                    <input type="radio" name="marital_status" id="divorced" value='2' class='marital-radio'>
                                    <label for="divorced">{{trans('home.divorced')}}</label>
                                    <span class="check"></span>
                                </div>
                                <div class="form-radio-item">
                                    <input type="radio" name="marital_status" id="sperated" value='3' class='marital-radio'>
                                    <label for="sperated">{{trans('home.separated')}}</label>
                                    <span class="check"></span>
                                </div>
                            </div>
                        </div>


                        <div class='spouse-area'>
                            <div class="form-row">
                                <div class="form-group">
                                    <label for="num_children">{{trans('home.no_of_children')}}</label>
                                    <input type="number" name="num_children" id="num_children" class='childRequired'/>
                                </div>
                            </div>
                            <hr class='spouse-hr'>
                            <div class='spouse-box'>
                                <div class="form-row">
                                    <div class="form-group">
                                        <label for="spouse_name">{{trans('home.spouse_name')}}</label>
                                        <input type="text" name="spouse_name" id="spouse_name" class='spouseRequired'/>
                                    </div>
                                    <div class="form-group">
                                        <label for="spouse_sex">{{trans('home.spouse_gender')}}</label>
                                        <select name="spouse_sex" id="spouse_sex" class='form-group select-form spouseRequired'>
                                            <option value=""></option>
                                            <option value="0">{{trans('home.male')}}</option>
                                            <option value="1">{{trans('home.female')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">

                                    <div class="form-group">
                                        <label for="spouse_occupation">{{trans('home.spouse_occupation')}}  </label>
                                        <input type="text" name="spouse_occupation" id="spouse_occupation" class='spouseRequired' />
                                    </div>
                                    <div class="form-group">
                                        <label for="spouse_ic">{{trans('home.spouse_ic')}}</label>
                                        <input type="text" name="spouse_ic" id="spouse_ic" class='spouseRequired' />
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group">
                                        <label for="spouse_mobile_phone_num">{{trans('home.spouce_mobile_no')}} </label>
                                        <input type="tel" name="spouse_mobile_phone_num" id="spouse_mobile_phone_num"  class='spouseRequired'/>
                                    </div>
                                    <div class="form-group">
                                        <label for="spouse_fixed_phone_num">{{trans('home.spouse_fix_phone_no')}}</label>
                                        <input type="tel" name="spouse_fixed_phone_num" id="spouse_fixed_phone_num"  class='spouseRequired'/>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="spouse_age">{{trans('home.spouse_age')}}</label>
                                        <input type="number" name="spouse_age" id="spouse_age" class='spouseRequired' />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="spouse_height">{{trans('home.spouse_height')}} </label>
                                        <input type="number" name="spouse_height" id="spouse_height" class='spouseRequired' />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="spouse_weight">{{trans('home.spouse_weight')}} </label>
                                        <input type="number" name="spouse_weight" id="spouse_weight" class='spouseRequired' />
                                    </div>
                                </div>
                                <div class="form-textarea address-field">
                                    <label for="pre_existing_cond" class="radio-label">{{trans('home.spouse_pre_condition')}}</label>
                                    <textarea name="pre_existing_cond" id="pre_existing_cond"  class='spouseRequired'></textarea>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <h3>
                        {{trans('home.nomination')}}
                    </h3>
                    <fieldset>

                        <p class='nominee-title'> {{trans('home.nominee_1')}} </p>

                        <div class="form-row">
                            <div class="form-group">
                                <label for="nominee_1_name"  class='required-span'>{{trans('home.name')}}</label>
                                <input type="text" name="nominee_1_name" id="nominee_1_name" class="required"/>
                            </div>
                            <div class="form-group">
                                <label for="nominee_1_sex"  class='required-span'>{{trans('home.gender')}}</label>
                                <select name="nominee_1_sex" class='form-group select-form required' id='nominee_1_sex'>
                                    <option value=""></option>
                                    <option value="0">{{trans('home.male')}}</option>
                                    <option value="1">{{trans('home.female')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">

                            <div class="form-group">
                                <label for="nominee_1_age"  class='required-span'>{{trans('home.age')}}  </label>
                                <input type="number" name="nominee_1_age" id="nominee_1_age"  class="required"/>
                            </div>
                            <div class="form-group">
                                <label for="nominee_1_ic"  class='required-span'>{{trans('home.ic_no')}}</label>
                                <input type="text" name="nominee_1_ic" id="nominee_1_ic" class="required" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="nominee_1_mobile_phone_num"  class='required-span'>{{trans('home.mobile_no')}} </label>
                                <input type="tel" name="nominee_1_mobile_phone_num" id="nominee_1_mobile_phone_num" class="required"/>
                            </div>
                            <div class="form-group">
                                <label for="nominee_1_fixed_phone_num"  class='required-span'>{{trans('home.fix_phone_no')}}</label>
                                <input type="tel" name="nominee_1_fixed_phone_num" id="nominee_1_fixed_phone_num" class="required"/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="nominee_1_relation"  class='required-span'>{{trans('home.relation_to_insured')}} </label>
                                <input type="text" name="nominee_1_relation" id="nominee_1_relation" class="required"/>
                            </div>
                            <div class="form-group">
                                <label for="nominee_1_policy_payout"  class='required-span'>% of Policy payout</label>
                                <input type="tel" name="nominee_1_policy_payout" id="nominee_1_policy_payout" class="required"/>
                            </div>
                        </div>
                        <div class="form-textarea address-field">
                            <label for="nominee_1_address" class="radio-label required-span">{{trans('home.address')}} </label>
                            <textarea name="nominee_1_address" id="nominee_1_address"  class="required"></textarea>
                        </div>

                        <hr class='nominee-hr'>
                        <p class='nominee-title'> {{trans('home.nominee_2')}} </p>

                        <div class="form-row">
                            <div class="form-group">
                                <label for="nominee_2_name"  class='required-span'>{{trans('home.name')}} </label>
                                <input type="text" name="nominee_2_name" id="nominee_2_name" class="required"/>
                            </div>
                            <div class="form-group">
                                <label for="nominee_2_sex"  class='required-span'>{{trans('home.gender')}} </label>
                                <select name="nominee_2_sex" class='form-group select-form required' id='nominee_2_sex'>
                                    <option value=""></option>
                                    <option value="0">{{trans('home.male')}} </option>
                                    <option value="1">{{trans('home.female')}} </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">

                            <div class="form-group">
                                <label for="nominee_2_age"  class='required-span'>{{trans('home.age')}}   </label>
                                <input type="number" name="nominee_2_age" id="nominee_2_age"  class="required"/>
                            </div>
                            <div class="form-group">
                                <label for="nominee_2_ic"  class='required-span'>{{trans('home.ic_no')}} </label>
                                <input type="text" name="nominee_2_ic" id="nominee_2_ic"  class="required"/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="nominee_2_mobile_phone_num"  class='required-span'>{{trans('home.mobile_no')}}  </label>
                                <input type="tel" name="nominee_2_mobile_phone_num" id="nominee_2_mobile_phone_num" class="required" />
                            </div>
                            <div class="form-group">
                                <label for="nominee_2_fixed_phone_num"  class='required-span'>{{trans('home.fix_phone_no')}} </label>
                                <input type="tel" name="nominee_2_fixed_phone_num" id="nominee_2_fixed_phone_num" class="required" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="nominee_2_relation"  class='required-span'>{{trans('home.relation_to_insured')}}  </label>
                                <input type="text" name="nominee_2_relation" id="nominee_2_relation" class="required" />
                            </div>
                            <div class="form-group">
                                <label for="nominee_2_policy_payout"  class='required-span'>% of Policy payout</label>
                                <input type="tel" name="nominee_2_policy_payout" id="nominee_2_policy_payout" class="required" />
                            </div>
                        </div>
                        <div class="form-textarea address-field">
                            <label for="nominee_2_address" class="radio-label required-span">{{trans('home.address')}} </label>
                            <textarea name="nominee_2_address" id="nominee_2_address" class="required" ></textarea>
                        </div>
                    </fieldset>

                    <h3>
                        Payment
                    </h3>
                    <fieldset>
                        <p class='payment-title required-span'> Please select your payment method </p>
                        <div class="paymentWrap">
			                   <div class="btn-group paymentBtnGroup btn-group-justified" data-toggle="buttons">
			                      <label class="btn paymentMethod active">
          	                  <div class="method visa"></div>
                              <input type="radio" name="options" checked>
                            </label>
      					            <label class="btn paymentMethod">
      					            	<div class="method master-card"></div>
    					                <input type="radio" name="options">
      					            </label>
          					      </div>
          						</div>
                    </fieldset>
                {!! Form::close() !!}
            </div>

        </div>

      </section>
  </div>
</main>
@endsection
