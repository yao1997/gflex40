@extends("layout.welcome_layout")

@section('content')
<div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel" data-interval="1000000">

<!--Indicators
<ol class="carousel-indicators">
  <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
  <li data-target="#carousel-example-1z" data-slide-to="1"></li>
  <li data-target="#carousel-example-1z" data-slide-to="2"></li>
</ol>
/.Indicators-->

<!--Slides-->
<div class="carousel-inner" role="listbox">

  <!--First slide-->
  <div class="carousel-item  {{ ((Request::route()->getName()=='index')?'active':' ')}}">
    <div class="view" style="background-image: url('/img/home.jpg'); background-repeat: no-repeat; background-size: cover;">

      <!-- Mask & flexbox options-->
      <div class="mask rgba-black-strong d-flex justify-content-center align-items-center">

        <!-- Content -->
        <div class="text-center white-text mx-5 wow fadeIn text-center-padding">
          <h1 class="mb-4">
            <strong>GFlex40</strong>
          </h1>

          <p>
            <strong>{{trans('home.introduction')}}</strong>
          </p>

          <p class="mb-4 d-none d-md-block">
            <strong>
              {{trans('home.introduction_description')}}
              <br/>
              {{trans('home.for_all_malaysian')}}
            </strong>
          </p>

          <a class="btn btn-outline-white btn-lg" href='/form'>{{trans('home.register')}}
            <i class="fas fa-heart ml-2"></i>
          </a>
        </div>
        <!-- Content -->

      </div>
      <!-- Mask & flexbox options-->

    </div>
  </div>
  <!--/First slide-->

  <!--Second slide-->
  <div class="carousel-item  {{ ((Request::route()->getName()=='about_us')?'active':' ')}}">
    <div class="view" style="background-image: url('/img/about_us.jpg'); background-repeat: no-repeat; background-size: cover;">

      <!-- Mask & flexbox options-->
      <div class="mask rgba-black-strong d-flex justify-content-center align-items-center">

        <!-- Content -->
        <div class="text-center white-text mx-5 wow fadeIn text-center-padding">
          <h1 class="mb-4">
            <strong>{{trans('home.about_us')}}</strong>
          </h1>

          <p>
            <strong>{{trans('home.about_us_1')}}</strong>
          </p>

          <p class="mb-4 d-none d-md-block">
            <strong>
              {{trans('home.about_us_2')}}
              <br/>
              {{trans('home.about_us_3')}}
              <br/>
              {{trans('home.about_us_4')}}
            </strong>
          </p>

          <a target="_blank" data-toggle="modal" data-target="#elegantModalForm" class="btn btn-outline-white btn-lg">{{trans('home.register')}}
            <i class="fas fa-graduation-cap ml-2"></i>
          </a>
        </div>
        <!-- Content -->

      </div>
      <!-- Mask & flexbox options-->

    </div>
  </div>
  <!--/Second slide-->

  <!--Third slide-->
  <div class="carousel-item  {{ ((Request::route()->getName()=='what_is_takaful')?'active':' ')}}">
    <div class="view" style="background-image: url('/img/what_is.jpg'); background-repeat: no-repeat; background-size: cover;">

      <!-- Mask & flexbox options-->
      <div class="mask rgba-black-strong d-flex justify-content-center align-items-center">

        <!-- Content -->
        <div class="text-center white-text mx-5 wow fadeIn text-center-padding">
          <h1 class="mb-4">
            <strong>{{trans('home.what_is_takaful')}}</strong>
          </h1>

          <p>
            <strong>{{trans('home.what_is_takaful_page')}}</strong>
          </p>

          <a target="_blank" data-toggle="modal" data-target="#elegantModalForm" class="btn btn-outline-white btn-lg">{{trans('home.register')}}
            <i class="fas fa-graduation-cap ml-2"></i>
          </a>
        </div>
        <!-- Content -->

      </div>
      <!-- Mask & flexbox options-->

    </div>
  </div>
  <!--/Third slide-->

</div>
<!--/.Slides-->

<!--Controls
<a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  <span class="sr-only">{{trans('home.next')}}</span>
</a>
<a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
  <span class="carousel-control-next-icon" aria-hidden="true"></span>
  <span class="sr-only">{{trans('home.prev')}}</span>
</a>
/.Controls-->

</div>

<main>
  <div class="container">
      <div class="container">

        <h3 class="my-5 h3 text-center">{{trans('home.pricing_plan')}}</h3>

        <!--Grid row-->
        <div class="row d-flex justify-content-center">

          <!--Grid column-->
          <div class="col-md-10 text-center">

            <p class="mb-5">
              {{trans('home.pricing_plan_description')}}
              <br/>{{trans('home.pricing_plan_description2')}}
            </p>

          </div>
          <!--Grid column-->

        </div>
        <!--Grid row-->

        <!--Grid row-->
        <div class="row text-center wow fadeIn">

          <!--Grid column-->
          <div class="col-lg-4 col-md-12 mb-4">

            <!--Card-->
            <div class="card">

              <!-- Card header -->
              <div class="card-header">
                <h4>
                  <strong>Takaful 30K</strong>
                </h4>
              </div>

              <!--Card content-->
              <div class="card-body">

                <h3 class="card-title pricing-card-title mb-4">{{trans('home.as_low_as_10')}}
                  <small class="text-muted">{{trans('home.per_month')}}</small>
                </h3>

                <ol class="list-unstyled mb-4">
                  <li>{{trans('home.starter_pack')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.RM30000')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.immediate_approval')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.flexible')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.simple')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                </ol>

                <button type="button" class="btn btn-lg btn-block btn-primary waves-effect">{{trans('home.sign_up_now')}}</button>

              </div>

            </div>
            <!--/.Card-->

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-4 col-md-6 mb-4">

            <!--Card-->
            <div class="card">

              <!-- Card header -->
              <div class="card-header">
                <h4>
                  <strong>Takaful 50K</strong>
                </h4>
              </div>

              <!--Card content-->
              <div class="card-body">

                <h3 class="card-title pricing-card-title mb-4">{{trans('home.as_low_as_30')}}
                  <small class="text-muted">{{trans('home.per_month')}}</small>
                </h3>

                <ol class="list-unstyled mb-4">
                  <li>{{trans('home.family_pack')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.RM50000')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.immediate_approval')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.flexible')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.simple')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                </ol>

                <button type="button" class="btn btn-lg btn-block btn-outline-primary waves-effect">{{trans('home.sign_up_now')}}</button>

              </div>

            </div>
            <!--/.Card-->

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-4 col-md-6 mb-4">

            <!--Card-->
            <div class="card">

              <!-- Card header -->
              <div class="card-header">
                <h4>
                  <strong>Takaful 100k</strong>
                </h4>
              </div>

              <!--Card content-->
              <div class="card-body">

                <h3 class="card-title pricing-card-title mb-4">{{trans('home.as_low_as_60')}}
                  <small class="text-muted">{{trans('home.per_month')}}</small>
                </h3>

                <ol class="list-unstyled mb-4">
                  <li>{{trans('home.professional_pack')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.RM100000')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.immediate_approval')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.flexible')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                  <hr>
                  <li>{{trans('home.simple')}}
                    <i class="fas fa-check green-text ml-1"></i>
                  </li>
                </ol>

                <button type="button" class="btn btn-lg btn-block btn-primary waves-effect">{{trans('home.sign_up_now')}}</button>

              </div>

            </div>
            <!--/.Card-->

          </div>
          <!--Grid column-->

        </div>
        <!--Grid row-->

      </div>

      <section class="pt-5">

        <h2 class="my-5 h3 text-center">{{trans('home.why_us')}}</h2>

        <!--First row-->
        <div class="row features-small mb-5 mt-3 wow fadeIn">

          <div class="col-md-4">

            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x indigo-text"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">{{trans('home.world_first')}}</h6>
                <p class="grey-text">
                    {{trans('home.world_first_description')}}
                </p>
                <div style="height:15px"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x indigo-text"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">{{trans('home.fully_sharia_compliant')}}</h6>
                <p class="grey-text">
                    {{trans('home.fully_sharia_compliant_desciption')}}
                </p>
                <div style="height:15px"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x indigo-text"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">{{trans('home.immediate_approval')}}</h6>
                <p class="grey-text">
                    {{trans('home.immediate_approval_description')}}
                </p>
                <div style="height:15px"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x indigo-text"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">{{trans('home.affordable')}}</h6>
                <p class="grey-text">
                    {{trans('home.affordable_description')}}
                </p>
                <div style="height:15px"></div>
              </div>
            </div>
          </div>
          <!--/First column-->

          <!--Second column-->
          <div class="col-md-4 flex-center">
            <img src="/css/img/welcome_whyus.JPG" alt="MDB Magazine Template displayed on iPhone" class="z-depth-0 img-fluid">
          </div>
          <!--/Second column-->

          <!--Third column-->
          <div class="col-md-4 mt-2">

            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x indigo-text"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">{{trans('home.easy_to_access')}}</h6>
                <p class="grey-text">
                    {{trans('home.easy_to_access_description')}}
                </p>
                <div style="height:15px"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x indigo-text"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">{{trans('home.convenient')}}</h6>
                <p class="grey-text">
                    {{trans('home.convenient_desciption')}}
                </p>
                <div style="height:15px"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x indigo-text"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">{{trans('home.easy_payment')}}</h6>
                <p class="grey-text">
                    {{trans('home.easy_payment_desciption')}}
                </p>
                <div style="height:15px"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-2">
                <i class="fas fa-check-circle fa-2x indigo-text"></i>
              </div>
              <div class="col-10">
                <h6 class="feature-title">{{trans('home.flexible')}}</h6>
                <p class="grey-text">
                    {{trans('home.flexible_desciption')}}
                </p>
                <div style="height:15px"></div>
              </div>
            </div>

          </div>
          <!--/Third column-->

        </div>
        <!--/First row-->

      </section>
  </div>
</main>
@endsection
