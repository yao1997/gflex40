<!-- Modal -->
<div class="modal fade" id="elegantModalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <!--Content-->
    <div class="modal-content form-elegant">
      <!--Header-->
      <div class="modal-header text-center">
        <h3 class="modal-title w-100 dark-grey-text font-weight-bold my-3" id="myModalLabel"><strong>Subscribe Us</strong></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--Body-->
			{!! Form::open(['route' => 'subscribe','class'=>'hero-subscribe-from']) !!}
      <div class="modal-body mx-4">
        <div class="md-form mb-5">
          <input type="email" id="Form-email1" name="email" class="form-control validate">
          <label data-error="invalid email" for="Form-email1">Your email</label>
        </div>

        <div class="text-center mb-3">
          <button type="submit" class="btn blue-gradient btn-block btn-rounded z-depth-1a subscribe-btn">Subscribe</button>
        </div>
      </div>
			{!! Form::close() !!}

      <div class="modal-footer mx-5 pt-3 mb-1">
        <p class="font-small grey-text d-flex justify-content-end">
          By subscribing us, you will be able to get the latest news.
        </p>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->
