<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>GFlex40</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="/css/mdb/bootstrap.min.css" rel="stylesheet">
  <link href="/css/mdb/mdb.min.css" rel="stylesheet">
  <link href="/css/mdb/style.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/css/plugin/toastr.min.css">
  <link href="/css/page/welcome_index.css" rel="stylesheet">
  <style type="text/css">
    @media (min-width: 800px) and (max-width: 850px) {
      .navbar:not(.top-nav-collapse) {
        background: #1C2331 !important;
      }
    }
  </style>

    <script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="/js/popper.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/page/main.js"></script>

  @yield('head')
</head>

<body>


    <!--Loader section -->
    <div class='page-loader'>
        <div class='loader'>
            <img src="/img/icon/loader.gif"/>
        </div>
    </div>
    <!--Loader section end -->


  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <div class="container">

      <!-- Brand -->
      <a class="navbar-brand" href="/" >
        <strong>GFlex40</strong>
      </a>

      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- Left -->
        <ul class="navbar-nav mr-auto">

        </ul>

        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item {{ ((Request::route()->getName()=='index')?'active':' ')}}">
              <a class="nav-link" href="/">{{trans('home.home')}}
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item {{ ((Request::route()->getName()=='about_us')?'active':' ')}}">
              <a class="nav-link " href="/about_us" >
                {{trans('home.about_us')}}
              </a>
            </li>
            <li class="nav-item {{ ((Request::route()->getName()=='what_is_takaful')?'active':' ')}}">
              <a class="nav-link " href="/what_is_takaful">
                {{trans('home.what_is_takaful')}}
              </a>
            </li>
            <li class="nav-item">
              <div class="dropdown">
                <a class="dropdown-toggle nav-link" id="dropdownMenu1" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="false">Language</a>

                <!--Menu-->
                <div class="dropdown-menu dropdown-primary">
                  <a class="dropdown-item" href="/en">English</a>
                  <a class="dropdown-item" href="/ms">Bahasa Melayu</a>
                </div>
              </div>
            </li>
            <li class="nav-item">
              <a href="/form" class="nav-link border border-light rounded {{ ((Request::route()->getName()=='form')?'active':' ')}}"
               >
                {{trans('home.register')}}
              </a>
            </li>
        </ul>

      </div>

    </div>
  </nav>
  <!-- Navbar -->

  @yield('content')


  <!--Footer-->
  <footer class="page-footer text-center font-small mt-4 wow fadeIn">

    <!--Call to action-->
    <div class="pt-4">
      <a class="btn btn-outline-white" data-toggle="modal" data-target="#elegantModalForm"
        role="button">Subscribe Us
        <i class="fas fa-bell ml-2"></i>
      </a>
    </div>
    <!--/.Call to action-->

    <hr class="my-4">

    <!-- Social icons -->
    <div class="pb-4">
      <a href="#" target="_blank">
        <i class="fab fa-facebook-f mr-3"></i>
      </a>

      <a href="#" target="_blank">
        <i class="fab fa-twitter mr-3"></i>
      </a>

      <a href="#" target="_blank">
        <i class="fab fa-youtube mr-3"></i>
      </a>

      <a href="#" target="_blank">
        <i class="fab fa-google-plus-g mr-3"></i>
      </a>

      <a href="#" target="_blank">
        <i class="fab fa-dribbble mr-3"></i>
      </a>

      <a href="#" target="_blank">
        <i class="fab fa-pinterest mr-3"></i>
      </a>

      <a href="#" target="_blank">
        <i class="fab fa-github mr-3"></i>
      </a>

      <a href="#" target="_blank">
        <i class="fab fa-codepen mr-3"></i>
      </a>
    </div>
    <!-- Social icons -->

    <!--Copyright-->
    <div class="footer-copyright py-3">
      © 2019 Copyright:
      <a href="#" target="_blank"> GFlex40 </a>
    </div>
    <!--/.Copyright-->

  </footer>
  <!--/.Footer-->

  <script type="text/javascript" src="/js/mdb.min.js"></script>
  <script type="text/javascript"  src="/js/plugin/toastr.min.js"></script>
  <script type="text/javascript">
    new WOW().init();
    toastr.options = {
      "debug": false,
      "positionClass": "toast-bottom-right",
      "onclick": null,
      "fadeIn": 300,
      "fadeOut": 1000,
      "timeOut": 5000,
      "extendedTimeOut": 1000
    }
  </script>
  <script>
  @if (Session::has("success"))
      toastr.success('{{Session::get("success")}}');
      @endif
  @if ($errors->any()) {
      @foreach ($errors->all() as $error)
          toastr.error('{{$error}}');
      @endforeach
  }
  @endif
  </script>
</body>

@include('modal.subscribe')
</html>
