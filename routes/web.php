<?php

Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath']], function(){
  Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);
  Route::get('/about_us', ['as' => 'about_us', 'uses' => 'IndexController@index']);
  Route::get('/what_is_takaful', ['as' => 'what_is_takaful', 'uses' => 'IndexController@index']);
  Route::post('/subscribe',['as'=>'subscribe','uses'=>'IndexController@subscribe']);

  Route::get('/form', ['as' => 'form', 'uses' => 'FormController@index']);
  Route::post('/form/submit',['as'=>'form.submit','uses'=>'FormController@submit']);
});
