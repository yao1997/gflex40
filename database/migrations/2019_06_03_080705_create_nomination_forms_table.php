<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNominationFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nomination_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('participant_form_id');
            $table->string('name')->nullable();
            $table->unsignedInteger('sex')->comment('0 - Male, 1- Female, 2 - Others')->nullable();
            $table->integer('age')->nullable();
            $table->string('address')->nullable();
            $table->string('ic')->nullable();
            $table->string('mobile_phone_num')->nullable();
            $table->string('fixed_phone_num')->nullable();
            $table->string('relation')->nullable();
            $table->decimal('policy_payout')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nomination_forms');
    }
}
