<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participant_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->unsignedInteger('sex')->comment('0 - Male, 1- Female, 2 - Others')->nullable();
            $table->unsignedInteger('marital_status')->comment('0 - Single, 1- Married, 2 - Divorced, 3 - Seperated')->nullable();
            $table->integer('age')->nullable();
            $table->string('occupation')->nullable();
            $table->string('ic')->nullable();
            $table->string('mobile_phone_num')->nullable();
            $table->string('fixed_phone_num')->nullable();
            $table->decimal('height')->nullable();
            $table->decimal('weight')->nullable();

            $table->integer('num_children')->nullable();
            $table->text('address')->nullable();
            $table->text('major_surgery')->nullable();

            $table->string('spouse_name')->nullable();
            $table->integer('spouse_age')->nullable();
            $table->unsignedInteger('spouse_sex')->comment('0 - Male, 1- Female, 2 - Others')->nullable();
            $table->string('spouse_occupation')->nullable();
            $table->string('spouse_ic')->nullable();
            $table->string('spouse_mobile_phone_num')->nullable();
            $table->string('spouse_fixed_phone_num')->nullable();
            $table->decimal('spouse_height')->nullable();
            $table->decimal('spouse_weight')->nullable();
            $table->string('pre_existing_cond')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participant_forms');
    }
}
